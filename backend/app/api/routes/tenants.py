from typing import Any

from fastapi import APIRouter, HTTPException
from sqlmodel import func, select

from app.api.deps import CurrentUser, SessionDep
from app.models import Tenant, TenantCreate, TenantOut, TenantsOut, TenantUpdate, Message

router = APIRouter()


@router.get("/tenants", response_model=TenantsOut)
def read_items(
    session: SessionDep, current_user: CurrentUser, skip: int = 0, limit: int = 100, show_all: bool = False
) -> Any:
    """
    Retrieve items.
    """
    query = select(Tenant.city).distinct()
    result = session.exec(query)
    cities = [city for city in result.all()]
    if current_user.is_superuser:
        count_statement = select(func.count()).select_from(Tenant)
        count = session.exec(count_statement).one()
        statement = select(Tenant).offset(skip).limit(limit)
        items = session.exec(statement).all()
    else:
        if show_all:
            count_statement = (
                select(func.count())
                .select_from(Tenant)
                # .where(Tenant.owner_id == current_user.id)
            )
        else:
            count_statement = (
                select(func.count())
                .select_from(Tenant)
                .where(Tenant.owner_id == current_user.id)
            )
            
        count = session.exec(count_statement).one()
        if show_all:
            statement = (
                select(Tenant)
                # .where(Tenant.owner_id == current_user.id)
                .offset(skip)
                .limit(limit)
            )
        else:
            statement = (
                select(Tenant)
                .where(Tenant.owner_id == current_user.id)
                .offset(skip)
                .limit(limit)
            )
        items = session.exec(statement).all()

    return TenantsOut(data=items, count=count,cities=cities)


@router.get("/tenant/{id}", response_model=TenantOut)
def read_item(session: SessionDep, current_user: CurrentUser, id: int) -> Any:
    """
    Get item by ID.
    """
    item = session.get(Tenant, id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not current_user.is_superuser and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    return item


@router.post("/tenant", response_model=TenantOut)
def create_item(
    *, session: SessionDep, current_user: CurrentUser, item_in: TenantCreate
) -> Any:
    """
    Create new item.
    """
    # 检查该用户已经有多少记录
    existing_items_count = session.query(Tenant).filter(Tenant.owner_id == current_user.id).count()
    if not current_user.is_superuser and existing_items_count >= 2:
        raise HTTPException(status_code=400, detail="User can only have up to 2 items")

    # 限制通过后，继续创建新记录
    
    item = Tenant.model_validate(item_in, update={"owner_id": current_user.id})
    session.add(item)
    session.commit()
    session.refresh(item)
    return item


@router.put("/tenant/{id}", response_model=TenantOut)
def update_item(
    *, session: SessionDep, current_user: CurrentUser, id: int, item_in: TenantUpdate
) -> Any:
    """
    Update an item.
    """
    item = session.get(Tenant, id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not current_user.is_superuser and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    update_dict = item_in.model_dump(exclude_unset=True)
    item.sqlmodel_update(update_dict)
    session.add(item)
    session.commit()
    session.refresh(item)
    return item


@router.delete("/tenant/{id}")
def delete_item(session: SessionDep, current_user: CurrentUser, id: int) -> Message:
    """
    Delete an item.
    """
    item = session.get(Tenant, id)
    if not item:
        raise HTTPException(status_code=404, detail="Item not found")
    if not current_user.is_superuser and (item.owner_id != current_user.id):
        raise HTTPException(status_code=400, detail="Not enough permissions")
    session.delete(item)
    session.commit()
    return Message(message="Item deleted successfully")
