from fastapi import APIRouter, Depends
from pydantic.networks import EmailStr
import os

from app.api.deps import get_current_active_superuser, SessionDep
from app.models import Message, ImageMessage
from app.utils import generate_test_email, send_email
import requests
from fastapi import FastAPI, File, UploadFile
import httpx
import time
import http.client
import mimetypes
from codecs import encode
from fastapi.responses import JSONResponse
from pydantic import BaseModel



router = APIRouter()


@router.post(
    "/test-email/",
    dependencies=[Depends(get_current_active_superuser)],
    status_code=201,
)
def test_email(email_to: EmailStr) -> Message:
    """
    Test emails.
    """
    email_data = generate_test_email(email_to=email_to)
    send_email(
        email_to=email_to,
        subject=email_data.subject,
        html_content=email_data.html_content,
    )
    return Message(message="Test email sent")


@router.post("/upload-image/", status_code=201)
# def upload_image(file: UploadFile = File(...), session: SessionDep) -> Message:
def upload_image(smfile: UploadFile) -> ImageMessage:
    """
    Upload an image.
    """

    file_name = smfile.filename
    file_type = smfile.content_type
    # Store file to /tmp/image folder
    if not os.path.exists("/tmp/image"):
        os.makedirs("/tmp/image")
    with open(f"/tmp/image/{smfile.filename}", "wb") as buffer:
        buffer.write(smfile.file.read())

#     conn = http.client.HTTPSConnection("sm.ms")
    dataList = []
    boundary = 'wL36Yn8afVp8Ag7AmP8qZ0SA4n1v9T'
    dataList.append(encode('--' + boundary))
    dataList.append(encode('Content-Disposition: form-data; name=smfile; filename={0}'.format(file_name)))
    dataList.append(encode('Content-Type: {}'.format(file_type)))
    dataList.append(encode(''))

    with open(f'/tmp/image/{file_name}', 'rb') as f:
       dataList.append(f.read())
    dataList.append(encode('--'+boundary+'--'))
    dataList.append(encode(''))
    body = b'\r\n'.join(dataList)
    payload = body
    headers = {
       'Authorization': 'caNRpk8uXRgFgd6NELt2Oi6fxKpxiTyU',
       'Content-type': 'multipart/form-data; boundary={}'.format(boundary)
    }
#     conn.request("POST", "/api/v2/upload", payload, headers)
#     res = conn.getresponse()
#     data = res.read()

    url = "https://sm.ms/api/v2/upload"
    response = requests.request("POST", url, headers=headers, data=payload)

    print(response.json())
    response_json = response.json()
#     print(response_json)
    if response_json["code"] == "image_repeated":
        return ImageMessage(message="OK", image_url=response_json["images"])
    if response_json["code"] != "success":
        raise HTTPException(status_code=400, detail=response_json["message"])

    image_url = response_json["data"]["url"]

    # Garbage Clean
    # Remove file in /tmp path
    if os.path.exists(f"/tmp/image/{file_name}"):
        os.remove(f"/tmp/image/{file_name}")

    return ImageMessage(message="OK", image_url=image_url)

#     url = "https://api.imgbb.com/1/upload?key=e36b65c3c0364bcc6c7d27c234278094"
#     with open(f"/tmp/image/{smfile.filename}", "rb") as buffer:
#         file_content = buffer.read()
#     print(file_content)
#     files = [
#         ('image', (file_name, file_content, file_type))
#     ]
#     print(files)
#     time.sleep(5)
#     headers = {
#         'Content-Type': 'multipart/form-data'
#     }
#     response = requests.post(url, headers=headers, files=files)
#
#     print(response.json())
#     print(response.json()["data"]["url"])
