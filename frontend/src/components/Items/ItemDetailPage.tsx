import {
    Box,
    Heading,
    Textarea,
    FormControl,
    Image,
    FormLabel,
    Input,
    Card, CardBody, Center,
} from "@chakra-ui/react";
import React from "react";
import {type ItemOut} from "../../client";

interface ItemPageDetailProps {
    itempage: ItemOut;
}

const ItemDetailPage: React.FC<ItemPageDetailProps> = ({itempage}) => {
    return (
        <>
            <Center>
                <Card direction={{base: 'column', sm: 'row'}}
                      overflow='hidden'
                      variant='outline'
                      align='center'
                      w={"60%"}
                      m={4}
                >
                    <CardBody>
                        <Center>
                            <Heading>房源信息</Heading>
                        </Center>
                        <br/>
                        <div>
                            <Center>
                                <Box width="60%" height="0" paddingBottom="50%" objectFit='cover' position="relative">
                                    {itempage.image_url ? (<Image data-type='Image'
                                                                  src={itempage.image_url}
                                                                  alt='Green double couch with wooden legs'
                                                                  borderRadius='lg'
                                                                  position="absolute"
                                                                  width="100%"
                                                                  height="100%"
                                                                  objectFit="cover"></Image>) :
                                        (<Image data-type='Image'
                                                src='https://s2.loli.net/2024/04/16/dmk5fRvlDcyoszY.png'
                                                alt='Green double couch with wooden legs'
                                                borderRadius='lg'
                                                position="absolute"
                                                width="100%"
                                                height="100%"
                                                objectFit="cover"></Image>)
                                    }
                                </Box>
                            </Center>
                            <br/>
                            <FormControl mt={4}>
                                <FormLabel>房源类型</FormLabel>
                                <Input value={itempage.property_type === "student" ? "学生公寓": itempage.property_type === "social" ? "社会公寓" : '' || ''} placeholder="学生公寓/社会公寓" type="text"
                                       isReadOnly/>
                            </FormControl>


                            <FormControl mt={4}>
                                <FormLabel>房间类型</FormLabel>
                                <Input value={itempage.room_type || ''} placeholder="studio/ensuite/1b1b" type="text"
                                       isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>所在城市</FormLabel>
                                <Input value={itempage.city ? itempage.city : "NA"} type="text" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>公寓名称</FormLabel>
                                <Input value={itempage.title ? itempage.title : "NA"} type="text" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>邮编</FormLabel>
                                <Input value={itempage.post_code || ''} placeholder="邮编" type="text" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>价格（GBP pw）</FormLabel>
                                <Input value={itempage.price?.toString() || ''} placeholder="价格" type="number"
                                       isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>起租日期</FormLabel>
                                <Input value={itempage.begin_date || ''} type="date" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>结束日期</FormLabel>
                                <Input value={itempage.end_date || ''} type="date" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>是否欢迎拆分租期短租</FormLabel>
                                <Input
                                    value={itempage.short_term_allowed === true ? "看情况" : '不欢迎'}
                                    placeholder="是否欢迎短租" type="text" isReadOnly/>
                            </FormControl>


                            <FormControl mt={4}>
                                <FormLabel>房间描述</FormLabel>
                                <Textarea value={itempage.description || ''} placeholder="房间描述" isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>价格描述</FormLabel>
                                <Textarea value={itempage.price_description?.toString() || ''} placeholder="价格描述"
                                          isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>时长描述</FormLabel>
                                <Textarea value={itempage.duration_description?.toString() || ''} placeholder="时长描述"
                                          isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>额外信息</FormLabel>
                                <Textarea value={itempage.additional_info || ''} placeholder="额外信息"
                                          isReadOnly/>
                            </FormControl>

                            <FormControl mt={4}>
                                <FormLabel>联系邮箱</FormLabel>
                                <Input value={itempage.email || ''} placeholder="邮箱" type="text" isReadOnly/>
                            </FormControl>
                                    <FormControl mt={4}>
                        <FormLabel>其他联系方式</FormLabel>
                        <Input value={itempage.contact_info || ''} placeholder="其他联系方式" type="text" isReadOnly />
                    </FormControl>


                            
                        </div>
                    </CardBody>
                </Card>
            </Center>
        </>
    )
}

export default ItemDetailPage
