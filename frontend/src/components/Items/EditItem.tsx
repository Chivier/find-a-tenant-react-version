import {
  Button,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Modal,
  Select,
  ModalBody,
  Textarea,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay, Image,
} from "@chakra-ui/react"
import React, {ChangeEvent, useEffect, useRef} from "react"
import { type SubmitHandler, useForm } from "react-hook-form"

import { useMutation, useQueryClient } from "react-query"
import {
  type ApiError,
  type ItemOut,
  type ItemUpdate,
  ItemsService, ImageMessage, UtilsService,
} from "../../client"
import useCustomToast from "../../hooks/useCustomToast"

interface EditItemProps {
  item: ItemOut
  isOpen: boolean
  onClose: () => void
}

interface FormDataPayload {
  formData: {
    smfile?: Blob | undefined;
  };
}

const EditItem: React.FC<EditItemProps> = ({ item, isOpen, onClose }) => {
  const queryClient = useQueryClient()
  const showToast = useCustomToast()
  const {
    register,
    setValue,
    handleSubmit,
    formState: { isSubmitting, errors },
  } = useForm<ItemUpdate>({
    mode: "onBlur",
    criteriaMode: "all",
    defaultValues: {
      title: "",
      city: "London",
      description: "",
      post_code: "",
      price: 0,
      begin_date: "",
      end_date: "",
      duration: 0,
      property_type: "",
      room_type: "",
      image_url: "",
      email: "",
      xhs_link:"",
      duration_description:"",
      price_description:"",
      short_term_allowed:false,
      additional_info:"",
      contact_info:"",
    },
  })

  const updateItem = async (data: ItemUpdate) => {
    await ItemsService.updateItem({ id: item.id, requestBody: data })
  }

  const mutation = useMutation(updateItem, {
    onSuccess: () => {
      showToast("Success!", "Item updated successfully.", "success")
      onClose()
    },
    onError: (err: ApiError) => {
      const errDetail = err.body?.detail
      showToast("Something went wrong.", `${errDetail}`, "error")
    },
    onSettled: () => {
      queryClient.invalidateQueries("items")
    },
  })

  const [imageUploaded, setImageUploaded] = React.useState<boolean>(item.image_url !== '' ? true : false);
  const [file, setFile] = React.useState<File | null>(null);
  const [image, setImage] = React.useState<string>(item.image_url ?? '');
  const [errorMessage, setErrorMessage] = React.useState<string>('');

  const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.files) {
      return;
    }
    setFile(event.target.files[0]);
  };

  const handleUpload = async () => {
    if (!file) {
      setErrorMessage('Please select an image file');
      return;
    }
    try {
      const data = new FormData();
      data.append('smfile', file);
      const formDataPayload: FormDataPayload = {
        formData: {
          smfile: data.get('smfile') as Blob | undefined,
        },
      };
      // @ts-ignore
      const response: ImageMessage = await UtilsService.uploadImage(formDataPayload);
      console.log('Upload successful:', response);
      setImageUploaded(true); // Update state using the setImageUploaded function
      const url = response.image_url;
      console.log('Image URL:', url);
      setImage(url);
      setValue('image_url', url);

      console.log('image uploaded:', imageUploaded); // This might not be updated immediately
      return response;
    } catch (error) {
      console.error('Upload error:', error);
      return '';
    }
  };

  useEffect(() => {
    setValue("title", item.title)
    setValue("city", item.city)
    setValue("description", item.description)
    setValue("post_code", item.post_code)
    setValue("price", item.price)
    setValue("begin_date", item.begin_date)
    setValue("end_date", item.end_date)
    setValue("duration", item.duration)
    setValue("property_type", item.property_type)
    setValue("room_type", item.room_type)
    setValue("image_url", item.image_url)
    setValue("email", item.email)
    setValue("xhs_link", item.xhs_link)
    setValue("duration_description", item.duration_description)
    setValue("price_description", item.price_description)
    setValue("short_term_allowed", item.short_term_allowed)
    setValue("additional_info", item.additional_info)
    setValue("contact_info", item.contact_info)
    if (file) {
      handleUpload();
    }
  }, [file]);

  const fileInputRef = useRef<HTMLInputElement>(null);

  const onSubmit: SubmitHandler<ItemUpdate> = async (data) => {
    mutation.mutate(data)
  }

  return (
    <>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        size={{ base: "sm", md: "md" }}
        isCentered
      >
        <ModalOverlay />
        <ModalContent as="form" onSubmit={handleSubmit(onSubmit)}>
          <ModalHeader>房源信息</ModalHeader>
          <ModalCloseButton />

          <ModalBody pb={6}>

          <FormControl isRequired isInvalid={!!errors.property_type} mt={4}>
              <FormLabel htmlFor="property_type">房源类型</FormLabel>
              <Select
                  id="property_type"
                  {...register("property_type",{
                    required: "property_type is required.",
                  })}
                  placeholder="选择房源类型"
                  
              >
              <option value="student">学生公寓</option>
              <option value="social">社会公寓</option>
              </Select>
              {errors.property_type && (
                  <FormErrorMessage>{errors.property_type.message}</FormErrorMessage>
              )}
            </FormControl>

            <FormControl isRequired isInvalid={!!errors.room_type} mt={4}>
              <FormLabel htmlFor="room_type">房间类型</FormLabel>
              <Input
                  id="room_type"
                  {...register("room_type",{
                    required: "room_type is required.",
                  })}
                  placeholder="studio/ensuite/1b1b/2b1b单间/2b1b整租/..."
                  type="text"
              />
              {errors.room_type && (
                  <FormErrorMessage>{errors.room_type.message}</FormErrorMessage>
              )}
            </FormControl>

            
            <FormControl isRequired isInvalid={!!errors.city} mt={4}>
              <FormLabel htmlFor="city">所在城市</FormLabel>
              <Select
                  id="city"
                  {...register("city",{

                    required: "city is required.",
                  })}
                  placeholder="选择城市"
              >
                <option value="Aberdeen">阿伯丁</option>,
                <option value="Cambridge">剑桥</option>,
                <option value="Exeter">埃克塞特</option>,
                <option value="Edinburgh">爱丁堡</option>,
                <option value="Birmingham">伯明翰</option>,
                <option value="Brighton">布莱顿</option>,
                <option value="Bristol">布里斯托</option>,
                <option value="Durham">杜伦</option>,
                <option value="Glasgow">格拉斯哥</option>,
                <option value="Cardiff">卡迪夫</option>,
                <option value="Coventry">考文垂</option>,
                <option value="Leicester">莱斯特</option>,
                <option value="Lancaster">兰卡斯特</option>,
                <option value="Liverpool">利物浦</option>,
                <option value="Leeds">利兹</option>,
                <option value="London">伦敦</option>,
                <option value="Oxford">牛津</option>,
                <option value="Reading">雷丁</option>,
                <option value="Manchester">曼彻斯特</option>,
                <option value="Southampton">南安普顿</option>,
                <option value="Newcastle">纽卡斯尔</option>,
                <option value="Nottingham">诺丁汉</option>,
                <option value="Portsmouth">朴茨茅斯</option>,
                <option value="Plymouth">普利茅斯</option>,
                <option value="Sunderland">桑德兰</option>,
                <option value="Sheffield">谢菲尔德</option>,
                <option value="York">约克</option>,
                <option value="others"> 其他</option>,
              </Select>
              {errors.city && (
                  <FormErrorMessage>{errors.city.message}</FormErrorMessage>
              )}

            </FormControl>

            <FormControl isRequired isInvalid={!!errors.title} mt={4}>
              <FormLabel htmlFor="title">公寓名称</FormLabel>
              <Input
                  id="title"
                  {...register("title", {
                    required: "Title is required.",
                  })}
                  placeholder="学生公寓名称/社会公寓街区"
                  type="text"
              />
              {errors.title && (
                  <FormErrorMessage>{errors.title.message}</FormErrorMessage>
              )}
            </FormControl>
            
            <FormControl isRequired isInvalid={!!errors.post_code} mt={4}>
              <FormLabel htmlFor="post_code">邮编</FormLabel>
              <Input
                  id="post_code"
                  {...register("post_code",{
                    required: "post_code is required.",
                  })}
                  placeholder="邮编"
                  type="text"
              />
              {errors.post_code && (
                  <FormErrorMessage>{errors.post_code.message}</FormErrorMessage>
              )}
            </FormControl>

            <FormControl isRequired isInvalid={!!errors.price} mt={4}>
              <FormLabel htmlFor="price">价格（GBP pw）</FormLabel>
              <Input
                  id="price"
                  {...register("price",{
                    required: "price is required.",
                  })}
                  placeholder="请换算成每周价格，单位英镑"
                  type="number"
              />
              {errors.price && (
                  <FormErrorMessage>{errors.price.message}</FormErrorMessage>
              )}
            </FormControl>


            <FormControl isRequired isInvalid={!!errors.begin_date} mt={4}>
              <FormLabel htmlFor="begin_date">起租日期</FormLabel>
              <Input
                  id="begin_date"
                  {...register("begin_date",{
                    required: "begin_date is required.",
                  })}
                  placeholder="Begin_date"
                  type="date" // Use type="date" for date input
              />
              {errors.begin_date && (
                  <FormErrorMessage>{errors.begin_date.message}</FormErrorMessage>
              )}
            </FormControl>

            <FormControl isRequired isInvalid={!!errors.end_date} mt={4}>
              <FormLabel htmlFor="end_date">结束日期</FormLabel>
              <Input
                  id="end_date"
                  {...register("end_date",{
                    required: "end_date is required.",
                  })}
                  placeholder="End_date"
                  type="date" // Use type="date" for date input
              />
              {errors.end_date && (
                  <FormErrorMessage>{errors.end_date.message}</FormErrorMessage>
              )}
            </FormControl>

            <FormControl mt={4}>
              <FormLabel htmlFor="short_term_allowed">是否欢迎拆分租期短租？</FormLabel>
              <Select
                  id="short_term_allowed"
                  {...register("short_term_allowed")}
                  placeholder="选择是否欢迎短租"
              >
                <option value="true">看具体情况</option>
                <option value="false">不欢迎</option>
              </Select>
            </FormControl>

            <FormControl mt={4}>
            <FormLabel htmlFor="description">房间描述</FormLabel>
              <Textarea
                  id="description"
                  {...register("description")}
                  placeholder="简单介绍你的房间"
              />
            </FormControl>

            
            <FormControl mt={4}>
              <FormLabel htmlFor="price_description">价格描述</FormLabel>
              <Textarea
                  id="price_description"
                  {...register("price_description")}
                  placeholder="原价多少？是否可以还价？是否包bill？等等"

              />
            </FormControl>
            

            <FormControl mt={4}>
              <FormLabel htmlFor="duration_description">时长描述</FormLabel>
              <Textarea
                  id="duration_description"
                  {...register("duration_description")}
                  placeholder="最短租多久？最长租多久？严格要求还是可以商量？"
              />
            </FormControl>

            
            

            <FormControl mt={4}>
              <FormLabel htmlFor="additional_info">额外信息</FormLabel>
              <Textarea
                  id="additional_info"
                  {...register("additional_info")}
                  placeholder="其他内容"
              />
            </FormControl>
            
            <FormControl isRequired isInvalid={!!errors.email} mt={4}>
              <FormLabel htmlFor="email">联系邮箱</FormLabel>
              <Input
                  id="email"
                  {...register("email",{
                    required: "email is required.",
                  })}
                  placeholder="Email"
                  type="text"
              />
              {errors.email && (
                  <FormErrorMessage>{errors.email.message}</FormErrorMessage>
              )}
            </FormControl>
            
            <FormControl mt={4}>
              <FormLabel htmlFor="contact_info">其他联系方式</FormLabel>
              <Input
                  id="contact_info"
                  {...register("contact_info")}
                  placeholder="请谨慎留手机号/微信号，建议使用邮箱"
                  type="text"
              />

            </FormControl>


            

            <FormControl mt={4}>
              <FormControl mt={4}>
                <FormLabel htmlFor="image">上传封面图片</FormLabel>
                {imageUploaded ? (
                    <Image borderRadius="20" boxSize="full" src={image} />
                ) : null}
                <Input onChange={handleFileChange} multiple={false} ref={fileInputRef}  type="file" hidden />
                <Input {...register("image_url")} type="text" hidden />
                <br/>
                <Button onClick={() => fileInputRef?.current?.click()}  variant="primary">Select and Upload Image</Button>


              </FormControl>
              {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}
            </FormControl>

          </ModalBody>

          <ModalFooter gap={3}>
            <Button variant="primary" type="submit" isLoading={isSubmitting}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default EditItem
