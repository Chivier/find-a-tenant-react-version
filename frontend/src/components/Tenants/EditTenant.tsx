import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Modal,
  Select,
  Textarea,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from "@chakra-ui/react"
import type React from "react"
import { type SubmitHandler, useForm } from "react-hook-form"

import { useMutation, useQueryClient } from "react-query"
import {
  type ApiError,
  type TenantOut,
  type TenantUpdate,
  TenantsService,
} from "../../client"
import useCustomToast from "../../hooks/useCustomToast"

interface EditTenantProps {
  item: TenantOut
  isOpen: boolean
  onClose: () => void
}

const EditTenant: React.FC<EditTenantProps> = ({ item, isOpen, onClose }) => {
  const queryClient = useQueryClient()
  const showToast = useCustomToast()
  const {
    register,
    handleSubmit,
    formState: { isSubmitting },
  } = useForm<TenantUpdate>({
    mode: "onBlur",
    criteriaMode: "all",
    defaultValues: item,
  })

  const updateTenant = async (data: TenantUpdate) => {
    await TenantsService.updateItem({ id: item.id, requestBody: data })
  }

  const mutation = useMutation(updateTenant, {
    onSuccess: () => {
      showToast("Success!", "Tenant updated successfully.", "success")
      onClose()
    },
    onError: (err: ApiError) => {
      const errDetail = err.body?.detail
      showToast("Something went wrong.", `${errDetail}`, "error")
    },
    onSettled: () => {
      queryClient.invalidateQueries("items")
    },
  })

  const onSubmit: SubmitHandler<TenantUpdate> = async (data) => {
    mutation.mutate(data)
  }

  // const onCancel = () => {
  //   reset()
  //   onClose()
  // }

  return (
    <>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        size={{ base: "sm", md: "md" }}
        isCentered
      >
        <ModalOverlay />
        <ModalContent as="form" onSubmit={handleSubmit(onSubmit)}>
          <ModalHeader>Add Tenant</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
          <FormControl isRequired mt={4}>
              <FormLabel htmlFor="email">邮箱</FormLabel>
              <Input
                id="email"
                {...register("email")}
                placeholder="Email"
                type="text"
              />
            </FormControl>

            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="city">城市</FormLabel>
              <Select
                  id="city"
                  {...register("city",{

                    required: "city is required.",
                  })}
                  placeholder="选择城市"
              >
                <option value="Aberdeen">阿伯丁</option>,
                <option value="Cambridge">剑桥</option>,
                <option value="Exeter">埃克塞特</option>,
                <option value="Edinburgh">爱丁堡</option>,
                <option value="Birmingham">伯明翰</option>,
                <option value="Brighton">布莱顿</option>,
                <option value="Bristol">布里斯托</option>,
                <option value="Durham">杜伦</option>,
                <option value="Glasgow">格拉斯哥</option>,
                <option value="Cardiff">卡迪夫</option>,
                <option value="Coventry">考文垂</option>,
                <option value="Leicester">莱斯特</option>,
                <option value="Lancaster">兰卡斯特</option>,
                <option value="Liverpool">利物浦</option>,
                <option value="Leeds">利兹</option>,
                <option value="London">伦敦</option>,
                <option value="Oxford">牛津</option>,
                <option value="Reading">雷丁</option>,
                <option value="Manchester">曼彻斯特</option>,
                <option value="Southampton">南安普顿</option>,
                <option value="Newcastle">纽卡斯尔</option>,
                <option value="Nottingham">诺丁汉</option>,
                <option value="Portsmouth">朴茨茅斯</option>,
                <option value="Plymouth">普利茅斯</option>,
                <option value="Sunderland">桑德兰</option>,
                <option value="Sheffield">谢菲尔德</option>,
                <option value="York">约克</option>,
                <option value="others"> 其他</option>,
              </Select>
            </FormControl>

            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="post_code">区域/位置</FormLabel>
              <Input
                id="post_code"
                {...register("post_code")}
                placeholder="描述理想位置"
                type="text"
              />
            </FormControl>


            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="begin_date">开始日期</FormLabel>
              <Input
                id="begin_date"
                {...register("begin_date")}
                placeholder="Begin_date"
                type="date" // Use type="date" for date input
              />
            </FormControl>

            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="end_date">结束日期</FormLabel>
              <Input
                id="end_date"
                {...register("end_date")}
                placeholder="End_date"
                type="date" // Use type="date" for date input
              />
              
            </FormControl>


            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="price_upper_bound">每周预算上限</FormLabel>
              <Input
                id="price_upper_bound"
                {...register("price_upper_bound")}
                placeholder="0"
                type="number"
              />  
            </FormControl>

            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="property_type">房源类型</FormLabel>
              <Select
                  id="property_type"
                  {...register("property_type",{
                    required: "property_type is required.",
                  })}
                  placeholder="选择房源"
                  
              >
              <option value="student">学生公寓</option>
              <option value="social">社会公寓</option>
              <option value="any">不限</option>
              </Select>
            </FormControl>

            <FormControl mt={4}>
              <FormLabel htmlFor="room_type">房间类型</FormLabel>
              <Input
                id="room_type"
                {...register("room_type")}
                placeholder="studio/ensuite/1b1b/..."
                type="text"
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel htmlFor="let_type">求租类型</FormLabel>
              <Input
                id="let_type"
                {...register("let_type")}
                placeholder="官方/私下/均可"
                type="text"
              />
            </FormControl>
            <FormControl isRequired mt={4}>
              <FormLabel htmlFor="contact_info">联系方式</FormLabel>
              <Input
                id="contact_info"
                {...register("contact_info")}
                placeholder="推荐填写邮箱，谨慎填写微信/电话等"
                type="text"
              />
            </FormControl>
            <FormControl mt={4}>
              <FormLabel htmlFor="description">其他要求/信息</FormLabel>
              <Textarea
                id="description"
                {...register("description")}
                placeholder="其他要求/信息"
              />
            </FormControl>


            
            

          

            


          </ModalBody>

          <ModalFooter gap={3}>
            <Button variant="primary" type="submit" isLoading={isSubmitting}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  )
}

export default EditTenant
