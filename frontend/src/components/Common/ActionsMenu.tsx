import {
  Button,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  useDisclosure,
  useToast
} from "@chakra-ui/react"
import type React from "react"
import { BsThreeDotsVertical } from "react-icons/bs"
import { FiEdit, FiTrash, FiCopy } from "react-icons/fi"

import type { ItemOut, TenantOut, UserOut } from "../../client"
import EditUser from "../Admin/EditUser"
import EditItem from "../Items/EditItem"
import EditTenant from "../Tenants/EditTenant"
import Delete from "./DeleteAlert"

interface ActionsMenuProps {
  type: string
  value: ItemOut | UserOut
  disabled?: boolean
}

const ActionsMenu: React.FC<ActionsMenuProps> = ({ type, value, disabled }) => {
  const editUserModal = useDisclosure()
  const deleteModal = useDisclosure()
  console.log('value:', value)
  let detail_page_link: string = `https://find-a-tenant.com/item_detail/${value.id}`;
  const toast = useToast();

  const handleCopy = async () => {
    try {
      await navigator.clipboard.writeText(detail_page_link);
      toast({
        title: "已复制",
        description: "文本已成功复制到剪贴板。",
        status: "success",
        duration: 3000,
        isClosable: true,
        position: "top"
      });
    } catch (error) {
      toast({
        title: "复制失败",
        description: "无法复制文本。",
        status: "error",
        duration: 3000,
        isClosable: true,
        position: "top"
      });
    }
  };
  return (
    <>
      {/*@ts-ignore*/}
      <Menu display={editUserModal.isOpen ? "block" : "none"}>
        <MenuButton
          isDisabled={disabled}
          as={Button}
          rightIcon={<BsThreeDotsVertical />}
          variant="unstyled"
        />
        <MenuList
            right={0}>
          {type==='Flat' ? (<MenuItem onClick={handleCopy}
            icon={<FiCopy fontSize="16px" />}>复制链接</MenuItem>) : null}
          <MenuItem
            onClick={editUserModal.onOpen}
            icon={<FiEdit fontSize="16px" />}
          >
            编辑{type == 'Flat' ? '房源' : type == 'Item' ? '求租' : '用户'}
          </MenuItem>

          <MenuItem
            onClick={deleteModal.onOpen}
            icon={<FiTrash fontSize="16px" />}
            color="ui.danger"
          >
            删除{type == 'Flat' ? '房源' : type == 'Item' ? '求租' : '用户'}
          </MenuItem>
        </MenuList>
        {type === "User" ? (
          <EditUser
            user={value as UserOut}
            isOpen={editUserModal.isOpen}
            onClose={editUserModal.onClose}
          />
        ) : type === "Flat" ? (
          <EditItem
            item={value as ItemOut}
            isOpen={editUserModal.isOpen}
            onClose={editUserModal.onClose}
          />
        ) : (
          <EditTenant
            item={value as TenantOut}
            isOpen={editUserModal.isOpen}
            onClose={editUserModal.onClose}
          />
        
        )}
        <Delete
          type={type}
          id={value.id}
          isOpen={deleteModal.isOpen}
          onClose={deleteModal.onClose}
        />
      </Menu>
    </>
  )
}

export default ActionsMenu
