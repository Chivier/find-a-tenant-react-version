import { Box, Flex, Icon, Text, useColorModeValue } from "@chakra-ui/react"
import { Link } from "@tanstack/react-router"
import type React from "react"
import {FiBriefcase, FiHome, FiSettings, FiUsers, FiCompass, FiMap} from "react-icons/fi"
import { useQueryClient } from "react-query"

import type { UserOut } from "../../client"

const items = [
  { icon: FiHome, title: "所有房源", path: "/" },
  { icon: FiHome, title: "所有求租", path: "/tenant" },
  { icon: FiBriefcase, title: "房源管理", path: "/items" },
  { icon: FiBriefcase, title: "求租管理", path: "/tenant_post" },
  { icon: FiCompass, title: "分享卡片", path: "/about" },
  { icon: FiSettings, title: "用户设置", path: "/settings" },
  
  { icon: FiMap, title: "开发计划", path: "/milestone" },
]

interface SidebarItemsProps {
  onClose?: () => void
}

const SidebarItems: React.FC<SidebarItemsProps> = ({ onClose }) => {
  const queryClient = useQueryClient()
  const textColor = useColorModeValue("ui.main", "ui.white")
  const bgActive = useColorModeValue("#E2E8F0", "#4A5568")
  const currentUser = queryClient.getQueryData<UserOut>("currentUser")

  const finalItems = currentUser?.is_superuser
    ? [...items, { icon: FiUsers, title: "Admin", path: "/admin" }]
    : items

  const listItems = finalItems.map((item) => (
    <Flex
      as={Link}
      to={item.path}
      w="100%"
      p={2}
      key={item.title}
      activeProps={{
        style: {
          background: bgActive,
          borderRadius: "12px",
        },
      }}
      color={textColor}
      onClick={onClose}
    >
      <Icon as={item.icon} color={item.title=='分享卡片'? 'green.500':textColor}alignSelf="center" />
      <Text color={item.title=='分享卡片'? 'green.500':textColor} ml={2}>{item.title}</Text>
    </Flex>
  ))

  return (
    <>
      <Box>{listItems}</Box>
    </>
  )
}

export default SidebarItems
