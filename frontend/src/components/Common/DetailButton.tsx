import { Button, Flex, useDisclosure } from "@chakra-ui/react"
import type React from "react"
// import { FaPlus } from "react-icons/fa"
import { type ItemOut } from "../../client"

import ItemDetail from "../Items/ItemDetail"

interface DetailButtonProps {
  item: ItemOut;
}

const DetailButton: React.FC<DetailButtonProps> = ({ item }) => {
  const itemDetailModal = useDisclosure()

  return (
    <>
      <Flex p={0} gap={2}>
        {/* TODO: Complete search functionality */}
        {/* <InputGroup w={{ base: '100%', md: 'auto' }}>
                    <InputLeftElement pointerEvents='none'>
                        <Icon as={FaSearch} color='gray.400' />
                    </InputLeftElement>
                    <Input type='text' placeholder='Search' fontSize={{ base: 'sm', md: 'inherit' }} borderRadius='8px' />
                </InputGroup> */}
        
        <Button
          variant="primary"
          fontSize={{ base: "sm", md: "inherit" }}
          onClick={itemDetailModal.onOpen}
        >
          View Details
        </Button>
        <ItemDetail item={item} isOpen={itemDetailModal.isOpen} onClose={itemDetailModal.onClose} />
      </Flex>
    </>
  )
}

export default DetailButton
