/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $ImageMessage = {
    properties: {
        message: {
            type: 'string',
            isRequired: true,
        },
        image_url: {
            type: 'string',
            isRequired: true,
        },
    },
} as const;
