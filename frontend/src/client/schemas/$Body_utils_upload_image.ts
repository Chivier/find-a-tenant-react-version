/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export const $Body_utils_upload_image = {
    properties: {
        smfile: {
            type: 'binary',
            isRequired: true,
            format: 'binary',
        },
    },
} as const;
