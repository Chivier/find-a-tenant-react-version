/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ItemCreate = {
    email: string;
    image_url?: (string | null);
    xhs_link?: (string | null);
    city?: (string | null);
    title?: (string | null);
    property_type?: (string | null);
    room_type?: (string | null);
    post_code?: (string | null);
    begin_date?: (string | null);
    end_date?: (string | null);
    duration_description?: (string | null);
    price?: (number | null);
    price_description?: (string | null);
    description?: (string | null);
    short_term_allowed?: (boolean | null);
    additional_info?: (string | null);
    contact_info?: (string | null);
    duration?: (number | null);
};

