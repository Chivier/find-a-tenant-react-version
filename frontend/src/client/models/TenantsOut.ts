/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { TenantOut } from './TenantOut';

export type TenantsOut = {
    data: Array<TenantOut>;
    count: number;
    cities: Array<string>;
};

