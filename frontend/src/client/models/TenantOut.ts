/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type TenantOut = {
    email: string;
    city: string;
    post_code?: (string | null);
    distance?: (number | null);
    location?: (string | null);
    price_lower_bound?: (number | null);
    price_upper_bound?: (number | null);
    begin_date?: (string | null);
    end_date?: (string | null);
    property_type?: (string | null);
    room_type?: (string | null);
    description?: (string | null);
    let_type?: (string | null);
    contact_info?: (string | null);
    id: number;
    owner_id: number;
};

