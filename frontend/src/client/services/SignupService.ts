/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Message } from '../models/Message';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class SignupService {

    /**
     * Send Verification Code
     * Password Recovery
     * @returns Message Successful Response
     * @throws ApiError
     */
    public static sendVerificationCode({
        email,
        code,
    }: {
        email: string,
        code: string,
    }): CancelablePromise<Message> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/api/v1/sign-up/check/{email}/{code}',
            path: {
                'email': email,
                'code': code,
            },
            errors: {
                422: `Validation Error`,
            },
        });
    }

}
