import {createFileRoute} from "@tanstack/react-router"
import Dashboard from "../../components/Common/Dashboard"


export const Route = createFileRoute("/_layout/")({
    component: Dashboard,
})
