import {createFileRoute} from '@tanstack/react-router'
import {
    Box, Step, StepDescription, StepIcon, StepIndicator, StepNumber, Stepper, StepSeparator, StepStatus, StepTitle,
    useSteps, Container, Heading
} from "@chakra-ui/react"

export const Route = createFileRoute('/_layout/milestone')({
    component: Milestone
})

const steps = [
    {title: '基本功能上线', description: '网站上线'},
    {title: '支持邮件提醒，符合条件新房/新求租上新自动提醒', description: '稳定运营两个月'},
    {title: '支持多种排序检索功能，提升检索性能', description: '稳定运营三个月'},
    {title: '支持拼室友功能', description: '稳定运营四个月'},
    {title: 'AI 筛选功能上线（例如帮选「适合语言班」「适合 24 fall 新生」）', description: '稳定运营五个月'},
    {title: '支持小程序/公众号检索', description: '稳定运营六个月'},
    {title: '支持地图预览', description: '稳定运营九个月'},
    {title: '全面升级服务器', description: '广告收益达到 5000 英镑'},
    {title: '支持可信交易', description: '正式运营十八个月'},
]

function Milestone() {
    const {activeStep} = useSteps({
        index: 1,
        count: steps.length,
    })

    return (
        <>
            <Container maxW="full">
                <Box pt={12} m={8}>
                    <Heading>
                        开发计划
                    </Heading>
                    <br/>
                    <Stepper index={activeStep} orientation='vertical' height='700px' gap='0'>
            {steps.map((step, index) => (
                <Step key={index}>
                    <StepIndicator>
                        <StepStatus
                            complete={<StepIcon/>}
                            incomplete={<StepNumber/>}
                            active={<StepNumber/>}
                        />
                    </StepIndicator>

                    <Box flexShrink='0'>
                        <StepTitle>{step.title}</StepTitle>
                        <StepDescription>{step.description}</StepDescription>
                    </Box>

                    <StepSeparator/>
                </Step>
            ))}
        </Stepper>
                </Box>
            </Container>
        </>
    )
}

export default Milestone