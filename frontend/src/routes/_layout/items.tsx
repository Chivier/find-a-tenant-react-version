import {
  Container,
  Flex,
  Heading,
  Spinner,
  Table,
    Tag,
    TagLabel,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react"
import { createFileRoute } from "@tanstack/react-router"
import { useQuery } from "react-query"

import { type ApiError, ItemsService } from "../../client"
import ActionsMenu from "../../components/Common/ActionsMenu"
import Navbar from "../../components/Common/Navbar"
import useCustomToast from "../../hooks/useCustomToast"

export const Route = createFileRoute("/_layout/items")({
  component: Items,
})

function Items() {
  const showToast = useCustomToast()
  const {
    data: items,
    isLoading,
    isError,
    error,
  } = useQuery("items_post", () => ItemsService.readItems({showAll: false}))

  if (isError) {
    const errDetail = (error as ApiError).body?.detail
    showToast("Something went wrong.", `${errDetail}`, "error")
  }

  return (
    <>
      {isLoading ? (
        // TODO: Add skeleton
        <Flex justify="center" align="center" height="100vh" width="full">
          <Spinner size="xl" color="ui.main" />
        </Flex>
      ) : (
        items && (
          <Container maxW="full">
            <Heading
              size="lg"
              textAlign={{ base: "center", md: "left" }}
              pt={12}
            >
              房源管理
            </Heading>
            <br/>
            <Tag size={'lg'}
                 key={'lg'}
                 borderRadius='full'
                 variant='solid'
                 colorScheme='gray'>
              <TagLabel><b>
                添加房源结束后，请刷新页面检查是否添加成功
              </b></TagLabel>
            </Tag>
            <Navbar type={"Flat"} />
            <TableContainer>
              <Table size={{ base: "md", md: "md" }} marginBottom={40}>
                <Thead>
                  <Tr>
                    <Th>ID</Th>
                    <Th>公寓名称</Th>
                    <Th>价格</Th>
                    <Th>邮箱</Th>
                    <Th>起租日期</Th>
                    <Th>结束日期</Th>
                    <Th>编辑</Th>
                  </Tr>
                </Thead>
                <Tbody>
                  {items.data.map((item) => (
                    <Tr key={item.id}>
                      <Td>{item.id}</Td>
                      <Td>{item.title}</Td>
                      
                      <Td color={!item.price ? "gray.400" : "inherit"}>
                        {item.price || "N/A"}
                      </Td>

                      <Td color={!item.email ? "gray.400" : "inherit"}>
                        {item.email || "N/A"}
                      </Td>

                      <Td color={!item.begin_date ? "gray.400" : "inherit"}>
                        {item.begin_date || "N/A"}
                      </Td>
                      <Td color={!item.end_date ? "gray.400" : "inherit"}>
                        {item.end_date || "N/A"}
                      </Td>
                      <Td>
                        <ActionsMenu type={"Flat"} value={item} disabled={false}/>
                      </Td>
                    </Tr>
                  ))}
                </Tbody>
              </Table>
            </TableContainer>
          </Container>
        )
      )}
    </>
  )
}

export default Items
