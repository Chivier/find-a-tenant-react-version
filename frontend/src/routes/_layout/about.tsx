import {createFileRoute} from "@tanstack/react-router"
import {
    Heading, Card, CardBody, Box, Flex, Image, Text, Container,
    Center, OrderedList, ListItem, Divider, Highlight
} from "@chakra-ui/react";

export const Route = createFileRoute("/_layout/about")({
    component: AboutUs,
})

function AboutUs() {
    return (
        <>
            <Container maxW="full">
                <Box pt={12} m={2}>
                <Card>
                    <CardBody m={5}>
                        <Center>
                            <Heading>分享卡片</Heading>
                        </Center>
                        <br/>
                        <Flex justifyContent="center" gap="4">
                            <Image data-type='Image'
                                   src='https://i.ibb.co/5n285L9/cards.png'
                                   alt='Green double couch with wooden legs'
                                   boxSize="80%" objectFit="cover"></Image>
                        </Flex>
                        <Box textAlign="left" fontSize={"lg"} mt={8}>
                            <Text
                                fontSize={'xl'}>我们提供分享卡片服务！大家可以使用这张卡片在各种平台上发布自己的出租信息。</Text>
                        </Box>

                        <Box textAlign="left" fontSize={"lg"} mt={8}>
                            <Heading noOfLines={1} as='h3' size='lg'>
                                联系方式
                            </Heading>
                            <br/>联系邮箱: findatenant@163.com
                            <br/>小红书账号: 5032882531
                        </Box>
                        <br/>
                        <Divider/>
                        <br/>

                        <Box textAlign="left" fontSize={"lg"} mt={8}>
                            <Heading noOfLines={1} as='h3' size='lg'>
                                卡片获取
                            </Heading>
                            <br/>
                            <Text>您如果是第一批用户（即有效填写过在线表格），您的邮箱里应该已经收到了分享卡片。若未收到，请联系我们重发。</Text>
                            <Text>您如果不是第一批用户，仅需动动手指，两步即可获得您的分享卡片：</Text>
                            <OrderedList>
                                <ListItem>
                                    <Text>
                                        <Highlight
                                            query='内容中at我们'
                                            styles={{px: '2', py: '1', rounded: 'full', bg: 'red.100'}}
                                        >
                                            请在小红书上帮助我们曝光一次！具体操作是，发布一条关于转租的笔记，在内容中at我们；只要话题是关于转租的，无论帖子长短均可。
                                        </Highlight>
                                    </Text>
                                </ListItem>
                                <ListItem>

                                    <Text>
                                        <Highlight
                                            query={['发送邮件或者小红书私信', "截图"]}
                                            styles={{px: '2', py: '1', rounded: 'full', bg: 'red.100'}}
                                        >
                                            截图这次曝光（如相关笔记内容），发送邮件或者小红书私信告知我们，
                                        </Highlight>
                                    </Text>
                                </ListItem>
                            </OrderedList>
                            <br/>
                            <Text>
                                之后您会收到回复，包含您的分享卡片
                            </Text>
                        </Box>
                        <br/>
                        <Divider/>
                        <br/>

                        <Box textAlign="left" fontSize={"lg"} mt={8}>
                            <Heading noOfLines={1} as='h3' size='lg'>
                                关于我们
                            </Heading>
                            <br/>
                            我们的服务全程免费，意味着您不需要支付任何费用，也可以完成您的转租
                            <br/>
                            <br/>
                            <Heading noOfLines={1} as='h4' size='md' lineHeight='tall'>
                                <Highlight
                                    query={['上传门槛费', '成交手续费', '推流服务费']}
                                    styles={{px: '2', py: '1', rounded: 'full', bg: '#66ccffaa'}}
                                >
                                    没有上传门槛费!
                                </Highlight>
                            </Heading>
                            <Heading noOfLines={1} as='h4' size='md' lineHeight='tall'>
                                <Highlight
                                    query={['上传门槛费', '成交手续费', '推流服务费']}
                                    styles={{px: '2', py: '1', rounded: 'full', bg: '#66ccffaa'}}
                                >
                                    没有成交手续费!!
                                </Highlight>
                            </Heading>
                            <Heading noOfLines={1} as='h4' size='md' lineHeight='tall'>
                                <Highlight
                                    query={['上传门槛费', '成交手续费', '推流服务费']}
                                    styles={{px: '2', py: '1', rounded: 'full', bg: '#66ccffaa'}}
                                >
                                    没有推流服务费!!!
                                </Highlight>
                            </Heading>
                            <br/>
                            在这些基础功能上，您不需要花一分钱！
                            <br/>
                            <br/>因为我们知道一个简单的道理：
                            <br/>
                            <b>只有免费，参与的人才能多，只有参与的人多了，“转租”这件小事才能真正变得简单丝滑。</b>
                            <br/>
                            <br/>
                            我们不收钱，因为我们都曾为转租烦心过。
                            <br/>我们的目标不是赚快钱，而是解决问题！
                            <br/>倘若大家都能知道我们的存在，转租，对任何人而言，都会变得简单很多。
                            <br/>
                            <br/>
                            我们的开发用爱发电，所以成本不高（烧了很多头发），不过倘若体量上来了，也有不小的维护成本，和三方API开销。
                            <br/>
                            <br/>
                            但是无所谓，我们不会停下，我们的目标是帮助更多的人！乃至所有人！每一个人！
                            <br/>
                            <br/>
                            如果我们有帮助到您，欢迎点赞打赏！任意形式的好评都会给我们带来巨大动力！
                            <br/>
                            <br/>
                            最后，广告位长期招租！给孩子们恰点饭吧！
                        </Box>
                        <Box pt={12} m={2}>
                            <Flex justifyContent="center" gap="4">
                                <Image data-type='Image'
                                       src='https://s2.loli.net/2024/04/11/OAZvN2QEUW5Lkxp.jpg'
                                       alt='Green double couch with wooden legs'
                                       boxSize="150px" objectFit="cover"></Image>

                                <Image data-type='Image' src="https://s2.loli.net/2024/04/11/oj9vEg1MtSrXuix.png"
                                       alt="描述2" boxSize="150px" objectFit="cover"/>

                            </Flex>
                        </Box>

                    </CardBody>
                </Card>
                </Box>
            </Container>
        </>
    );
}

export default AboutUs;

