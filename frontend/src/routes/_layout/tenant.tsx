import {
    Container,
    Flex,
    Spinner,
    Heading,
    Box,
    Text,
    Select,
    CardFooter,
    Stack,
    Link,
    Image,
    Badge,
    CardBody,
    Grid,
    Card, Center, Highlight, GridItem, Input, Button
} from "@chakra-ui/react"
import {createFileRoute} from "@tanstack/react-router"
import {useQuery} from "react-query"
import {type ApiError, TenantOut, TenantsService} from "../../client"
import useCustomToast from "../../hooks/useCustomToast"
import DetailButton from "../../components/Common/TenantDetailButton"
import {useState} from 'react';


export const Route = createFileRoute("/_layout/tenant")({
    component: Dashboard,
})

const compareDates = (date1: string, date2: string): number => {
    const dateA = new Date(date1);
    const dateB = new Date(date2);
    return dateA.getTime() - dateB.getTime();
};
const isNumber = (value: string): boolean => {
    return !isNaN(Number(value));
};

function Dashboard() {
    const [selectedCity, setSelectedCity] = useState('');
    const [selectedBeginDate, setSelectedBeginDate] = useState('');
    const [selectedEndDate, setSelectedEndDate] = useState('');
    const [selectedPriceUpperBound, setSelectedPriceUpperBound] = useState('');
    const [errorinfo, setErrorinfo] = useState('');

    // const currentUser = queryClient.getQueryData<UserOut>("currentUser")
    const showToast = useCustomToast()
    const {
        data: tenants,
        isLoading,
        isError,
        error,
    } = useQuery("tenants", () => TenantsService.readItems({showAll: true, limit: 2000}))

    const [filteredItems, setFilteredItems] = useState([] as TenantOut[]);

    const updateFilter = () => {
        if (selectedBeginDate && selectedEndDate) {
            if (compareDates(selectedBeginDate, selectedEndDate) > 0) {
                setErrorinfo('开始日期不能晚于结束日期！');
                showToast("Something went wrong.", errorinfo, "error");
            }
        }
        if (selectedPriceUpperBound) {
            if (!isNumber(selectedPriceUpperBound)) {
                setErrorinfo('价格上限必须为数字！');
                showToast("Something went wrong.", errorinfo, "error");
            }
        }
        if (selectedPriceUpperBound && parseFloat(selectedPriceUpperBound) < 0) {
            setErrorinfo('价格不能为负数！');
            showToast("Something went wrong.", errorinfo, "error")
        }

        // @ts-ignore
        let next_filter = [...tenants.data];
        next_filter = next_filter.reverse();

        if (selectedCity) {
            // @ts-ignore
            next_filter = next_filter.filter(product => product.city === selectedCity);
        }

        if (selectedBeginDate) {
            // @ts-ignore
            next_filter = next_filter.filter(product => (compareDates(product.begin_date, selectedBeginDate) <= 0))
        }
        if (selectedEndDate) {
            // @ts-ignore
            next_filter = next_filter.filter(product => (compareDates(product.end_date, selectedEndDate) >= 0))
        }
        if (selectedPriceUpperBound) {
            const upperBound = parseFloat(selectedPriceUpperBound);
            // @ts-ignore
            next_filter = next_filter.filter(product => (product.price_upper_bound >= upperBound))
        }
        setFilteredItems(next_filter);
    }

    if (isError) {
        const errDetail = (error as ApiError).body?.detail
        showToast("Something went wrong.", `${errDetail}`, "error")
    }

    return (
        <>
            {isLoading ? (
                // TODO: Add skeleton
                <Flex justify="center" align="center" height="100vh" width="full">
                    <Spinner size="xl" color="ui.main"/>
                </Flex>
            ) : (
                tenants && (
                    <Container maxW="full">
                        <Box pt={12} m={0}>
                            <Center>
                            <Flex flexDirection={{base: "column", md: "row"}} justifyContent="center" gap="4">
                                <Link justifyContent="center" colorScheme='teal.500' href="https://shlink.chivier.site/jUcdt" target="_blank" rel="noopener noreferrer">
                                    <Center>
                                <Image data-type='Image'
                                            src ='https://s2.loli.net/2024/04/18/ASQEBxWtzO8dkbZ.jpg' 
                                            width={'85%'} height={'130px'}  objectFit="contain"></Image> 
                                            </Center>
                                    </Link>
                                    <Link justifyContent="center" colorScheme='teal.500' href="https://shlink.chivier.site/oWCLZ" target="_blank" rel="noopener noreferrer">
                                    <Center>
                                <Image data-type='Image'
                                            src ='https://s2.loli.net/2024/05/02/EnqlpZ8IPmLDKfW.jpg' 
                                            width={'85%'} height={'130px'}  objectFit="contain"></Image> 
                                            </Center>
                                    </Link>

                                    </Flex>
                            </Center>
                            <br/>

                            <Heading>
                                求租检索<Badge marginLeft={2} variant={'subtle'} colorScheme='facebook'>(选填)</Badge>
                            </Heading>

                            <br/>
                            <Box>
                                <Heading as='h4' size='md' marginY={2}>
                                    城市筛选
                                </Heading>
                                <Select placeholder="全部" onChange={(e) => setSelectedCity(e.target.value)}
                                        value={selectedCity}>
                                    {tenants.cities.map((city, index) => (
                                        <option key={index} value={city}>{city}</option>
                                    ))}
                                </Select>
                            </Box>
                            <Heading as='h4' size='md' marginY={2}>
                                日期筛选
                            </Heading>
                            <Grid
                                gap={2}
                                templateColumns='repeat(2, 1fr)'>
                                <GridItem>
                                    <Input placeholder='选择开始日期' size='md' type='date'
                                           value={selectedBeginDate}
                                           onChange={(e) => setSelectedBeginDate(e.target.value)}/>
                                </GridItem>
                                <GridItem>
                                    <Input placeholder='选择结束日期' size='md' type='date'
                                           value={selectedEndDate}
                                           onChange={(e) => setSelectedEndDate(e.target.value)}/>
                                </GridItem>
                            </Grid>
                            <Heading as='h4' size='md' marginY={2}>
                                每周预算高于：
                            </Heading>
                            <Grid
                                templateColumns='repeat(2, 1fr)'
                                gap={2}>
                                <Input placeholder={'价格'}
                                       value={selectedPriceUpperBound}
                                       onChange={(e) => {
                                           setSelectedPriceUpperBound(e.target.value)
                                       }}
                                />
                            </Grid>
                            <Grid
                                templateColumns='repeat(2, 1fr)'
                                gap={2}>
                                <Button marginY={4}
                                        variant="primary"
                                        onClick={
                                            () => {
                                                updateFilter()
                                            }
                                        }>
                                    检索
                                </Button>
                                <Button marginY={4}
                                        variant="primary"
                                        onClick={
                                            () => {
                                                setSelectedCity('');
                                                setSelectedBeginDate('');
                                                setSelectedEndDate('');
                                                setSelectedPriceUpperBound('');
                                                setErrorinfo('');
                                                let next_initial = tenants.data;
                                                next_initial = next_initial.reverse();
                                                setFilteredItems(next_initial);
                                            }
                                        }>
                                    清空检索条件
                                </Button>
                            </Grid>
                        </Box>


                        {
                            filteredItems?.length === 0 ? (
                                <Center>
                                    <Heading as='h4' size='md' marginY={2}>
                                        请输入或更换筛选条件
                                    </Heading>
                                </Center>
                            ) : (
                                <Grid templateColumns={{
                                    sm: "repeat(1, 1fr)",
                                    md: "repeat(2, 1fr)",
                                    lg: "repeat(3, 1fr)",
                                    xl: "repeat(4, 1fr)"
                                }} gap={6} m={2}>
                                    {filteredItems?.map((tenant) => (
                                        <Card key={tenant.id} data-type='Card' maxW='sm'>
                                            <CardBody data-type='CardBody' paddingBottom={0}>

                                                <Badge variant={'subtle'} colorScheme='facebook' px={2} py={1}
                                                       mt={2}>
                                                    <Heading data-type='Heading' size='md'>{tenant.city}</Heading>
                                                </Badge>

                                                <Stack data-type='Stack' mt='3' spacing='3'>
                                                    <Text datatype="Text">{tenant.post_code}</Text>
                                                </Stack>

                                                <Flex align="left" justify="left"
                                                      mt={1}> {/* Flex 容器保证内容在同一行 */}
                                                    <Badge colorScheme="blue" px={2} py={1}>
                                                        {tenant.begin_date} {/* 开始日期 */}
                                                    </Badge>
                                                    <Text mx={2}>~</Text> {/* 中间的连接符号，有适当的水平边距 */}
                                                    <Badge colorScheme="blue" px={2} py={1}>
                                                        {tenant.end_date} {/* 结束日期 */}
                                                    </Badge>
                                                </Flex>

                                                <Stack data-type='Stack' mt='3' spacing='3'>
                                                    <Text data-type='Text'>
                                                        {/*@ts-ignore*/}
                                                        {tenant.description?.length > 50 ? `${tenant.description.substring(0, 50)}...` : tenant.description || '无详细描述'}
                                                    </Text>
                                                    <Text data-type='Text' color='blue.600' fontSize='2xl'>
                                                        预算{tenant.price_upper_bound ? tenant.price_upper_bound > 0 ? tenant.price_upper_bound : '未给出' : '未给出'} pw
                                                    </Text>

                                                </Stack>
                                            </CardBody>
                                            <CardFooter data-type='CardFooter'>

                                                <DetailButton item={tenant}/>
                                            </CardFooter>
                                        </Card>
                                    ))}

                                </Grid>
                            )
                        }

                        <br/>
                        <Center>
                            <Heading as='h4' size='md'>
                                <Highlight
                                    query={['广告位长期招租']}
                                    styles={{px: '2', py: '1', rounded: 'full', bg: 'red.100'}}
                                >

                                    广告位长期招租
                                </Highlight>
                            </Heading>
                        </Center>
                        <br/>

                        <Center>
                            <Flex justifyContent="l" gap="4">


                                <Image data-type='Image'
                                       src='https://s2.loli.net/2024/04/11/OAZvN2QEUW5Lkxp.jpg'
                                       alt='Green double couch with wooden legs'
                                       boxSize="150px" objectFit="cover"></Image>

                                <Image data-type='Image' src="https://s2.loli.net/2024/04/11/oj9vEg1MtSrXuix.png"
                                       alt="描述2" boxSize="150px" objectFit="cover"/>
                            </Flex>
                        </Center>
                    </Container>
                )
            )}
        </>
    )
}

export default Dashboard

