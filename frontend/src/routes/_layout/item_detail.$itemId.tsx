import {
    Grid,
} from "@chakra-ui/react"
import {createFileRoute} from "@tanstack/react-router"
import { useQuery } from "react-query"
import  { type ApiError, ItemsService } from "../../client"
import useCustomToast from "../../hooks/useCustomToast.ts"
import ItemDetailPage from "../../components/Items/ItemDetailPage.tsx"

export const Route = createFileRoute("/_layout/item_detail/$itemId")({
    component: ItemDetailDashboard
})

function ItemDetailDashboard() {
    const { itemId } = Route.useParams()
    const itemIdNumber = parseInt(itemId)

    const showToast = useCustomToast()
    const {
        data: item_page_show,
        isError,
        error,
    } = useQuery("itempage", () => ItemsService.readItem({id: itemIdNumber}))

    const filteredItemPagesInfo = item_page_show;
    console.log(filteredItemPagesInfo);
    if (!filteredItemPagesInfo) {
        // Item not found page
        return <>
            <h1>
                Item not found!
            </h1>
        </>
    }

    if (isError) {
        const errDetail = (error as ApiError).body?.detail
        showToast("Something went wrong.", `${errDetail}`, "error")
    }

    return (
        <>
            <Grid
                alignItems="center"
                w={"100%"}
            >
            <ItemDetailPage itempage={item_page_show}/>
            </Grid>
        </>
    )
}

export default ItemDetailDashboard

